<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TimeZone"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/CalendarLogic.js"></script>
<link
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/ui-darkness/jquery-ui.css"
	rel="stylesheet">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>


</head>
<body>

	<h1>CSS Calendar</h1>

	<div class="month">
		<ul>
			<li class="prev">❮</li>
			<li class="next">❯</li>
			<li id="monthID" style="text-align: center">
				<% 
			
			out.println(new SimpleDateFormat("MMMMMMMMMMMM").format(Calendar.getInstance().getTime())); 
			%> <br> <span id="yearID" style="font-size: 18px"> <% out.println(Calendar.getInstance().get(Calendar.YEAR)); %>
			</span>
			</li>
		</ul>
	</div>

	<ul class="weekdays">
		<li>Mo</li>
		<li>Tu</li>
		<li>We</li>
		<li>Th</li>
		<li>Fr</li>
		<li>Sa</li>
		<li>Su</li>
	</ul>

	<ul class="days">

		<% 
			Calendar c = Calendar.getInstance(TimeZone.getDefault());
			String dayz = new SimpleDateFormat("d").format(Calendar.getInstance().getTime());
			int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			String calString = "<ul>";
			for (int dayIdx = 0; dayIdx < monthMaxDays; dayIdx++) {
				calString += "<li class='days'>";
				if (new Integer(dayz) == dayIdx) {
					calString += "<span class='active boxen'>"+ (dayIdx+1) + "</span></li>";
				} else {
					calString += "<span class='boxen'>" + (dayIdx+1) + "</span></li>";
				}
			}
			calString += "</ul>";	
			out.print(calString);  
		%>
	</ul>

	<div id="dialog" title="Save Event">
		<form action="" method="post">
			<label>Event Name:</label> <input id="eventName" name="eventName" type="text"> ]
			<label>Event Description:</label> 
			<textarea id="eventDesc" rows="4" cols="50"></textarea> 
			<input id="saveEvent" onClick="saveMe()" type="button" value="Save">
		</form>
	</div>

</body>
</html>