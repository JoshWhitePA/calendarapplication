package me.joshua_white.MySQLHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;

/**
 * The Class MySQLImplementation.
 *
 * @author Josh
 */


/**
 * 
 * Example
 * //		MySQLConnectionHandler imp = new MySQLConnectionHandler("192.168.0.6","superman","3827","Organization");
//		String query = "select * from Employees where id = ?";
//		ArrayList<Object> paramVals = new ArrayList<Object>();
//		
//		paramVals.add(new Integer(111));
//		ResultSet mySQLResults = imp.runQuery(query, paramVals);
//		
//		ArrayList<String> coulmnToRead = new ArrayList<String>();
//		coulmnToRead.add("id");
//		coulmnToRead.add("first");
//		coulmnToRead.add("last");
//		imp.readResults(mySQLResults, coulmnToRead);
//		
//		String createStatement = "create table Animal (Name varchar(100) primary key, Color varchar(100), Roar varchar(100))";
//		System.out.println(imp.runCreateTable(createStatement));
////		String insertQuery = "insert into Employees (id,age,first,last) values(?,?,?,?)";
////		ArrayList<Object>insertVals = new ArrayList<Object>();
////		insertVals.add(new Integer(111));
////		insertVals.add(new Integer(22));
////		insertVals.add(new String("Josh"));
////		insertVals.add(new String("White"));
////		System.out.println(imp.runInsert(insertQuery, insertVals) + "");
//		
//		
 *
 */
public class MySQLConnectionHandler {
	  
  	/** The conn. */
  	private Connection conn = null;
	  

	/**
 * Instantiates a new my sql implementation.
 *
 * @param host the host
 * @param user the user
 * @param passwd the passwd
 * @param databaseName the database name
 * @throws Exception the exception
 */
public MySQLConnectionHandler(String host,String user,String passwd,String databaseName) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
		conn = DriverManager.getConnection("jdbc:mysql://" + host + "/"+databaseName+"?" + "user=" + user + "&password=" + passwd );
	}
	

	/**
	 * Builds the query.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the prepared statement
	 * @throws SQLException the SQL exception
	 */
	private PreparedStatement buildQuery(String query, ArrayList<Object> paramVals) throws SQLException{
		PreparedStatement preparedStatement = conn.prepareStatement(query);
		for (int idx = 0; idx < paramVals.size(); idx++){
			switch(checkType(paramVals.get(idx))){
				case "Integer":
					preparedStatement.setInt(idx+1,furnishIntVal(paramVals.get(idx)));
					break;
				case "Double":
					preparedStatement.setDouble(idx+1, furnishDoubleVal(paramVals.get(idx)));
					break;
				case "String":
					preparedStatement.setString(idx+1, furnishStringVal(paramVals.get(idx)));
					break;
				case "Float":
					preparedStatement.setFloat(idx+1, furnishFloatVal(paramVals.get(idx)));
					break;
				case "Date":
					preparedStatement.setDate(idx+1, furnishDateVal(paramVals.get(idx)));
					break;
				case "Byte":
					preparedStatement.setByte(idx+1,furnishByteVal(paramVals.get(idx)));
					break;	
			}
		}
		return preparedStatement;
	}
	
	/**
	 * Run query.
	 *
	 * @param query the query
	 * @param paramVals the parameter values
	 * @return the result set
	 * @throws SQLException the SQL exception
	 */
	public ResultSet runQuery(String query, ArrayList<Object> paramVals) throws SQLException {
		PreparedStatement prepState = buildQuery(query, paramVals);
		ResultSet resultSet = prepState.executeQuery();
		
		return resultSet;
	}
	
	
	/**
	 * Runs query without any parameters.
	 *
	 * @param query the query
	 * @return the result set
	 * @throws SQLException the SQL exception
	 */
	public ResultSet runQuery(String query) throws SQLException {
		ResultSet resultSet = conn.prepareStatement(query).executeQuery();
		return resultSet;
	}
	
	
	/**
	 * Run insert.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public boolean runInsert(String query, ArrayList<Object> paramVals) throws SQLException {
		PreparedStatement prepState = buildQuery(query, paramVals);
		if(prepState.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Run insert.
	 *
	 * @param query the query
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public boolean runInsert(String query) throws SQLException {
		if(conn.prepareStatement(query).executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Run update.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runUpdate(String query, ArrayList<Object> paramVals) throws SQLException {
		int rowsAffected;
		PreparedStatement prepState = buildQuery(query, paramVals);
		rowsAffected = prepState.executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run update.
	 *
	 * @param query the query
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runUpdate(String query) throws SQLException {
		int rowsAffected;
		rowsAffected = conn.prepareStatement(query).executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run delete.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runDelete(String query, ArrayList<Object> paramVals) throws SQLException {
		int rowsAffected;
		PreparedStatement prepState = buildQuery(query, paramVals);
		rowsAffected = prepState.executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run delete.
	 *
	 * @param query the query
	 * @return rowsAffected the number of rows affected
	 * @throws SQLException the SQL exception
	 */
	public int runDelete(String query) throws SQLException {
		int rowsAffected;
		rowsAffected = conn.prepareStatement(query).executeUpdate();
		return rowsAffected;
	}
	
	 /**
 	 * Run create table.
 	 *
 	 * @param createStatement the create statement
 	 * @return true, if successful
 	 * @throws SQLException the SQL exception
 	 */
 	public boolean runCreateTable(String createStatement) throws SQLException {
		 return conn.prepareStatement(createStatement).execute();
	 }
	
 	/**
 	 * Read Results.
 	 *
 	 * @param resultSet the result set
 	 * @param key the key
 	 * @throws SQLException the SQL exception
 	 */
 	public void readResults(ResultSet resultSet, ArrayList<String> key ) throws SQLException {
		    // ResultSet is initially before the first data set
 			ArrayList<String> valuesByTag = new ArrayList<String>();
		    while (resultSet.next()) {
		    	for(int idx = 0;idx < key.size(); idx++){
		    		String value = resultSet.getString(key.get(idx));
		    		valuesByTag.add("Value of "+key.get(idx)+": "+value);
		    	}
		    }
		    for(String output:valuesByTag){
		    	System.out.println(output);
		    }
		  }
	 
	 /**
 	 * Display results.
 	 *
 	 * @param columnKey the column key
 	 * @param columnValue the column value
 	 */
 	private void displayResults(String columnKey, String columnValue){
		 System.out.println(columnKey +": " + columnValue);
	 }

	
	/**
	 * Furnish int val.
	 *
	 * @param intFromListObject the int from list object
	 * @return the integer
	 */
	private Integer furnishIntVal(Object intFromListObject){
		return (Integer) intFromListObject;
	}
	
	
	/**
	 * Furnish string val.
	 *
	 * @param stringFromListObject the string from list object
	 * @return the string
	 */
	private String furnishStringVal(Object stringFromListObject){
		return (String) stringFromListObject;
	}
	
	/**
	 * Furnish float val.
	 *
	 * @param floatFromListObject the float from list object
	 * @return the float
	 */
	private Float furnishFloatVal(Object floatFromListObject){
		return (Float) floatFromListObject;
	}
	
	/**
	 * Furnish double val.
	 *
	 * @param doubleFromListObject the double from list object
	 * @return the double
	 */
	private Double furnishDoubleVal(Object doubleFromListObject){
		return (Double) doubleFromListObject;
	}
	
	/**
	 * Furnish date val.
	 *
	 * @param dateFromListObject the date from list object
	 * @return the date
	 */
	private Date furnishDateVal(Object dateFromListObject){
		return (Date) dateFromListObject;
	}
	
	/**
	 * Furnish byte val.
	 *
	 * @param byteFromListObject the byte from list object
	 * @return Byte
	 */
	private Byte furnishByteVal(Object byteFromListObject){
		return (Byte) byteFromListObject;
	}
	
	
	
	/**
	 * Check type.
	 *
	 * @param checkee the checkee
	 * @return String
	 */
	private static String  checkType(Object checkee){
		if (checkee instanceof String){
			return "String";
		}else if(checkee instanceof Integer){
			return "Integer";
		}else if(checkee instanceof Float){
			return "Float";
		}else if(checkee instanceof Double){
			return "Double";
		}else if(checkee instanceof java.util.Date){
			return "Date";
		}else if(checkee instanceof Byte){
			return "Byte";
		}else{
			throw new IllegalArgumentException();
		}
	}
}

