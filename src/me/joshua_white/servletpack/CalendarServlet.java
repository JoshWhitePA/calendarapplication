package me.joshua_white.servletpack;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.joshua_white.MySQLHelper.*;

/**
 * Servlet implementation class CalendarServlet
 */
@WebServlet("/CalendarServlet")
public class CalendarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	MySQLConnectionHandler msch;
	
    /**
     * @throws Exception 
     * @see HttpServlet#HttpServlet()
     */
    public CalendarServlet() throws Exception {
        super();
        msch = new MySQLConnectionHandler("192.168.0.6","superman","3827","Calendar");
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String eventName = request.getParameter("eventName");
		String eventDesc = request.getParameter("eventDesc");
		String query = "insert into Events values(?,?,?,?,?)";
		ArrayList<Object> paramVals = new ArrayList<Object>();
		paramVals.add(new String(day));
		paramVals.add(new String(month));
		paramVals.add(new String(year));
		paramVals.add(new String(eventName));
		paramVals.add(new String(eventDesc));
		try {
			msch.runInsert(query, paramVals);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getWriter().append("Served at: "+day+" "+month+" "+year+" ").append(request.getContextPath());
	}

}
